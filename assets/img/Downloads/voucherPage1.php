<head>
    <meta charset="utf-8">
    <title>EShopper - Bootstrap Shop Template</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="Free HTML Templates" name="keywords">
    <meta content="Free HTML Templates" name="description">

    <!-- Favicon -->
    <link href="img/favicon.ico" rel="icon">

    <!-- Google Web Fonts -->
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">

    <!-- Font Awesome -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.0/css/all.min.css" rel="stylesheet">

    <!-- Libraries Stylesheet -->
    <link href="<?php echo base_url() ?>assets/lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">

    <!-- Customized Bootstrap Stylesheet -->
    <link href="<?php echo base_url() ?>assets/css/style.css" rel="stylesheet" />

    <link href="<?php echo base_url() ?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />

    <!-- Links for voucher page  -->

    <!-- <link src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css">

    </link> -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous" />

    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" />
    <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css" rel="stylesheet" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/fontawesome.min.css" rel="stylesheet"/> -->

    <!-- <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@fortawesome/fontawesome-free@5.15.4/css/fontawesome.min.css" integrity="sha384-jLKHWM3JRmfMU0A5x5AkjWkw/EYfGUAGagvnfryNV3F9VqM98XiIH7VBGVoxVSc7" crossorigin="anonymous"c/> -->


    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

    <!-- <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.bundle.min.js"></script> -->

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>


    <style>
        .btn-light {
            background-color: #e3f2ff;
            border-color: #e3f2ff;
        }

        #logoBanner {
            margin-left: 50px;
        }

        .m-5 {
            padding: 3rem !important;
        }

        .fs-12 {
            font-size: 12px;
        }

        .fs-14 {
            font-size: 14px;
        }

        .fs-16 {
            font-size: 16px;
        }

        .form-check {
            display: block;
            min-height: 1.5rem;
            padding-left: 1.5em;
            margin-bottom: 0.125rem;
        }

        .Bankcards {
            flex-direction: row;
        }

        .Bankcards img {
            width: 30%;
        }

        .cards-wrapper {
            display: flex;
        }

        .voucher {
            border-radius: 0px 40px 0 40px;
        }

        .InnerDiv {
            border-radius: 0px 40px 0 40px;
        }
    </style>



</head>

<body>


    <!-- Code for Brand logo -->
    <section style="background-color: #eee;">
        <div class="container-fluid px-5 py-1">
            <div class="row justify-content-center mb-3">
                <div class="col-md-12 col-xl-12">
                    <div class="card shadow-0 border rounded-3 ">
                        <div class="card-body">
                            <div class="row" id="logoBanner">
                                <div class="col-md-8 col-lg-8 col-xl-8">
                                    <h5>BigBasket</h5>
                                    <div class="d-flex flex-row">
                                        <span>Big Basket E-Gift Cards (Instant Vouchers)</span>
                                    </div>
                                    <div class="mb-2 text-muted small">
                                        <span class="text-primary"> • • • • •</span>
                                    </div>
                                    <p class="mb-4 mb-md-0">
                                        Getting groceries at home is made simpler by BigBasket.
                                        This online megastore offers farm fresh veggies, fruits, dairy products, and more to your doorstep.
                                        Discover daily essentials and gourmet food with 20k plus options to choose from.
                                        At Gifthos, we offer BigBasket gift cards at discounted prices for you.
                                        Redeem your gift card easily on your online orders.
                                </div>
                                <div class="col-md-3 col-lg-3 col-xl-3 border border-start">
                                    <div class="col-lg-4 col-md-12 mb-4">
                                        <div class="bg-image hover-zoom ripple shadow-1-strong rounded">
                                            <img src="assets/img/vendor-1.jpg" class="" />
                                            <a href="#!"></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Code for 4 buttons options -->
            <div class="row justify-content-center ">
                <div class="col-12 col-md-12 col-lg-12 col-xl-12 ml-0 m-2">
                    <div class=" card shadow-0 border ">
                        <div class="card-body">

                            <div class="btn-group col-12 ">
                                <button type="button" class="btn btn-outline-secondary mr-2 col"><i class="fa fa-file-text-o"></i>
                                    Instructions</button>
                                <button type="button" class="btn btn-outline-secondary mr-2 col"> <i class="fa fa-map-marker" aria-hidden="true"></i>
                                    Store Locator</button>
                                <button type="button" class="btn btn-outline-secondary mr-2 col"><i class="fa fa-book" aria-hidden="true"></i>T&C</button>
                                <button type="button" class="btn btn-outline-secondary mr-2 col"><i class="fa fa-question-circle" aria-hidden="true"></i>
                                    How to Redeem</button>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <!--  End of Code for 4 buttons options -->
        </div>


        <div class="container-fluid py-2 px-5">
            <!-- 1st line of coupons -->
            <div class="row pb-1">
                <div class="col-12 col-md-9 col-lg-9">
                    <div class="row">
                        <div class="col-4">
                            <div class="card voucher">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="bg-white p-2 position-relative h-100 fs-16 InnerDiv">
                                            <span class="h5 font-weight-bold">&#8377;50</span>
                                            <div class="p-2 mb-2 bg-light text-muted" style="width:auto">Discount 12.5</div>
                                            <div class="p-2 mb-2 bg-light text-muted" style="width:auto"> Saving</div>
                                            <div class="p-2 mb-2 bg-light text-muted" style="width:auto">You Pay</div>
                                            <a href="#" class="btn btn-light rounded-0 float-right">Add</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-4">
                            <div class="card voucher">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="bg-white p-2 position-relative h-100 fs-16 InnerDiv">
                                            <span class="h5 font-weight-bold">&#8377;100</span>
                                            <div class="p-2 mb-2 bg-light text-muted" style="width:auto">Discount </div>
                                            <div class="p-2 mb-2 bg-light text-muted" style="width:auto">Saving</div>
                                            <div class="p-2 mb-2 bg-light text-muted" style="width:auto">You Pay</div>
                                            <a href="#" class="btn btn-light float-right">Add</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-4">
                            <div class="card voucher">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="bg-white p-2 position-relative h-100 fs-16 InnerDiv">
                                            <span class="h5 font-weight-bold">&#8377;250</span>
                                            <div class="p-2 mb-2 bg-light text-muted" style="width:auto">Discount </div>
                                            <div class="p-2 mb-2 bg-light text-muted" style="width:auto">Saving</div>
                                            <div class="p-2 mb-2 bg-light text-muted" style="width:auto">You Pay</div>
                                            <a href="#" class="btn btn-light float-right">Add</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Cart section UI-->
                <div class="col-md-4 col-xl-3 ps-lg-0">
                    <div class="rightCart">
                        <div class="row m-0">
                            <div class="class=col-12 col-md-12 fs-18 py-2 bg-dark text-white fw-100">
                                <div class="summryHeader d-flex justify-content-between align-items-center py-1">
                                    <div>Cart</div>
                                </div>
                            </div>
                        </div>
                        <div class="m-0 py-3 border-bottom bg-white">
                            <div class="col-8 fs-14 fw-500 text-muted">
                                You Pay
                            </div>
                        </div>
                        <div class="m-0 py-3 border-bottom bg-white">
                            <div class="col-8 fs-14 fw-500 text-muted">
                                <div class="form-check">
                                    <input type="checkbox" class="form-check-input" id="exampleCheck1">
                                    <label class="form-check-label" for="exampleCheck1"><i class="fas fa-gift mr-2"></i>I am Gifting</label>
                                </div>

                            </div>
                        </div>
                        <div class="m-0 py-3 border-bottom bg-white">
                            <div class="col-8 fs-14 fw-500 text-muted">
                                <div class="form-check">
                                    <input type="checkbox" class="form-check-input" id="exampleCheck1">
                                    <label class="form-check-label" for="exampleCheck1"><i class="fa fa-whatsapp" aria-hidden="true"></i>
                                        Share on Whatsapp and Email</label>
                                </div>

                            </div>
                        </div>
                        <div class="p-md-3 d-grid bg-white">
                            <div class="btn py-3 py-md-2 btn-light rounded-0">
                                View/Edit Cart
                            </div>
                        </div>
                    </div>
                </div>

                <!--  End of Cart section UI-->

            </div>
             <!-- End of 1st line of coupons -->

             <!-- 2nd line of coupons -->
            <div class="row pb-1">
                <div class="col-12 col-md-9 col-lg-9">
                    <div class="row">
                        <!-- Each card -->
                        <div class="col-4 ">
                            <div class="card voucher">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="bg-white p-2 position-relative h-100 fs-16 InnerDiv">
                                            <span class="h5 font-weight-bold">&#8377;500</span>
                                            <div class="p-2 mb-2 bg-light text-muted" style="width:auto">Discount 12.5</div>
                                            <div class="p-2 mb-2 bg-light text-muted" style="width:auto"> Saving</div>
                                            <div class="p-2 mb-2 bg-light text-muted" style="width:auto">You Pay</div>
                                            <a href="#" class="btn btn-light float-right">Add</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Each card End-->

                        <div class="col-4">
                            <div class="card voucher">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="bg-white p-2 position-relative h-100 fs-16 InnerDiv">
                                            <span class="h5 font-weight-bold">&#8377;1000</span>
                                            <div class="p-2 mb-2 bg-light text-muted" style="width:auto">Discount </div>
                                            <div class="p-2 mb-2 bg-light text-muted" style="width:auto">Saving</div>
                                            <div class="p-2 mb-2 bg-light text-muted" style="width:auto">You Pay</div>
                                            <a href="#" class="btn btn-light float-right">Add</a>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <div class="col-4">
                            <div class="card voucher">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="bg-white p-2 position-relative h-100 fs-16 InnerDiv">
                                            <span class="h5 font-weight-bold">&#8377;2000</span>
                                            <div class="p-2 mb-2 bg-light text-muted" style="width:auto">Discount </div>
                                            <div class="p-2 mb-2 bg-light text-muted" style="width:auto">Saving</div>
                                            <div class="p-2 mb-2 bg-light text-muted" style="width:auto">You Pay</div>
                                            <a href="#" class="btn btn-light float-right">Add</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
             <!-- End of 2nd line of coupons -->


            <!-- Special bank offers -->
            <div class="BankOffers p-3">
                <h5>Special Bank Offers</h5>
                <div class="bg-white p-3 mobile-full-screen">
                    <div class="BankOffersOuter">
                        <div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
                            <div class="row">
                                <div class="col-md-12 col-xl-12">
                                    <div class="carousel-inner">
                                        <div class="carousel-item active">
                                            <div class="cards-wrapper">
                                                <div class="card Bankcards mr-2  col-12 col-md-4 col-xl-4" style="height: 100px;">
                                                    <img src="./assets/img/gifting.png" class="card-img-top" alt="...">
                                                    <div class="card-body">
                                                        <h5 class="card-title">Card title</h5>
                                                        <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                                                        <a href="#" class="btn btn-primary">Go somewhere</a>
                                                    </div>
                                                </div>


                                                <!-- <div class="carousel-item"> -->
                                                <div class="card Bankcards mr-2  col-12 col-md-4 col-xl-4" style="height: 100px;">
                                                    <img src="./assets/img/dining.png" class="card-img-top" alt="...">
                                                    <div class="card-body">
                                                        <h5 class="card-title">Card title</h5>
                                                        <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                                                        <a href="#" class="btn btn-primary">Go somewhere</a>
                                                    </div>
                                                </div>
                                                <!-- </div> -->

                                                <!-- <div class="carousel-item"> -->

                                                <div class="card Bankcards mr-2  col-12 col-md-4 col-xl-4" style="height: 100px;">
                                                    <img src="./assets/img/electronics.png" class="card-img-top" alt="...">
                                                    <div class="card-body">
                                                        <h5 class="card-title">Card title</h5>
                                                        <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                                                        <a href="#" class="btn btn-primary">Go somewhere</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End of Special bank offers -->
        </div>
    </section>

 

    <!-- Vendor Start -->
    <div class="container-fluid py-5">
        <div class="row px-xl-5">
            <div class="col">
                <div class="owl-carousel vendor-carousel">
                    <div class="vendor-item border p-4">
                        <img src="<?php echo base_url() ?>/assets/img/vendor-1.jpg" alt="">
                    </div>
                    <div class="vendor-item border p-4">
                        <img src="<?php echo base_url() ?>/assets/img/vendor-2.jpg" alt="">
                    </div>
                    <div class="vendor-item border p-4">
                        <img src="<?php echo base_url() ?>/assets/img/vendor-3.jpg" alt="">
                    </div>
                    <div class="vendor-item border p-4">
                        <img src="<?php echo base_url() ?>/assets/img/vendor-4.jpg" alt="">
                    </div>
                    <div class="vendor-item border p-4">
                        <img src="<?php echo base_url() ?>/assets/img/vendor-5.jpg" alt="">
                    </div>
                    <div class="vendor-item border p-4">
                        <img src="<?php echo base_url() ?>/assets/img/vendor-6.jpg" alt="">
                    </div>
                    <div class="vendor-item border p-4">
                        <img src="<?php echo base_url() ?>/assets/img/vendor-7.jpg" alt="">
                    </div>
                    <div class="vendor-item border p-4">
                        <img src="<?php echo base_url() ?>/assets/img/vendor-8.jpg" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Vendor End -->

    <!-- CDN Links  -->
    <!-- JavaScript Libraries -->
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js"></script>
    <script src="<?php echo base_url() ?>/assets/lib/easing/easing.min.js"></script>
    <script src="<?php echo base_url() ?>assets/lib/owlcarousel/assets/owl.carousel.min.css"></script>
    <script src="<?php echo base_url() ?>assets/lib/owlcarousel/owl.carousel.js"></script>

    <!-- Contact Javascript File -->
    <script src="<?php echo base_url() ?>/assets/mail/jqBootstrapValidation.min.js"></script>
    <script src="<?php echo base_url() ?>/assets/mail/contact.js"></script>

    <!-- Template Javascript -->
    <script src="<?php echo base_url() ?>/assets/js/main.js"></script>


</body>
</html>