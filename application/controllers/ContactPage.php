.<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ContactPage extends CI_Controller {

	
	public function index()
	{
		// $this->load->view('header');
		$this->load->view('contactPage');
		// $this->load->view('footer');
	}

	public function policy()
	{
		// $this->load->view('header');
		$this->load->view('privacyPolicy');
		// $this->load->view('footer');
	}

	public function faq()
	{
		// $this->load->view('header');
		$this->load->view('FAQ');
		// $this->load->view('footer');
	}

	public function tAndC()
	{
		// $this->load->view('header');
		$this->load->view('T&C');
		// $this->load->view('footer');
	}

	public function aboutus()
	{
		// $this->load->view('header');
		$this->load->view('AboutUs');
		// $this->load->view('footer');
	}
}
