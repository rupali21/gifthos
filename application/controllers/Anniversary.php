<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Anniversary extends CI_Controller {

	public function index()
	{
		$this->load->view('header');
		$this->load->view('anniversary');
		$this->load->view('footer');
	}
}
