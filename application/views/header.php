<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Gifthos</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="Free HTML Templates" name="keywords">
    <meta content="Free HTML Templates" name="description">

    <!-- Favicon -->
    <link href="<?php echo base_url() ?>/assets/img/favicon.ico" rel="icon">

    <!-- Google Web Fonts -->
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet"> 

    <!-- Font Awesome -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.0/css/all.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" />
 


    <!-- Libraries Stylesheet -->
    <link href="<?php echo base_url() ?>/assets/lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">

    <!-- Customized Bootstrap Stylesheet -->
    <link href="<?php echo base_url() ?>/assets/css/style.css" rel="stylesheet">

    <link href="<?php echo base_url() ?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />


    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.css" />
<!--  </head> -->
<style >
body{
    overflow-x: hidden !important;
}
.border-top {
    border-top:none !important; 
}
.input-group-text{
    border: 1px solid #ffffff;
}
        .heading {
    text-align: center;
    color: #454343;
    font-size: 30px;
    font-weight: 700;
    position: relative;
    margin-bottom: 70px;
    text-transform: uppercase;
    z-index: 999;
}
.white-heading{
    color: #ffffff;
}
/*.w-50 {
    width: 100% !important;
}*/
.w100{
    width: 96% !important;
    margin-left: 10px;
}
.heading:after {
    content: ' ';
    position: absolute;
    top: 100%;
    left: 50%;
    height: 40px;
    width: 180px;
    border-radius: 4px;
    transform: translateX(-50%);
    background: url(img/heading-line.png);
    background-repeat: no-repeat;
    background-position: center;
}
.white-heading:after {
    background: url(https://i.ibb.co/d7tSD1R/heading-line-white.png);
    background-repeat: no-repeat;
    background-position: center;
}

.heading span {
    font-size: 18px;
    display: block;
    font-weight: 500;
}
.white-heading span {
    color: #ffffff;
}

i.fa {
    display: inline-block;
    border-radius: 60px;
    box-shadow: 0 0 2px #888;
    padding: 0.4em 0.5em;
    font-size: 18px;
    margin: 5px;
    margin-bottom: 4px;

}
        .input-group {
    position: relative;
    display: flex;
    flex-wrap: wrap;
    align-items: stretch;
    width: 95%!important;
    /* margin-left: -63px; */
}
.text-right {
    text-align: right !important;
    margin-left: -237px;
}
/*.fa-stack-2x {
    font-size: 63px;
    margin-top: -43px;
    margin-left: -21px;
    /* border-style: outset; 
}*/

a {
    color: #1e1c1b;
    text-decoration: none;
    background-color: transparent;
}
.nav-link .fa.fa-user{
    padding: 0px;
    box-shadow: none;
}
.bottom-left-offer{
 position: absolute;
  bottom: 2px;
  right: 5px;
  font-size: 20px;
  margin-right: -6px;
}
.bottom-left-offers{
 position: absolute;
  bottom: 0px;
  right: 27px;
  top: 124px;
  font-size: 12px;
  margin-right: -6px;
  color: red;
}
.centered{
     position: absolute;
  top: 64%;
  left: 11%;
  transform: translate(-50%, -50%);
  font-size: 81%;
  color: black;
}
.navbar-expand-lg .navbar-nav .nav-link {
    padding: 8px 20px;
    text-align: center;
    font-size: 14px;
}
.btn-light {
            background-color: #e3f2ff;
            border-color: #e3f2ff;
        }

        #logoBanner {
            margin-left: 50px;
        }

        .m-5 {
            padding: 3rem !important;
        }

        .fs-12 {
            font-size: 12px;
        }

        .fs-14 {
            font-size: 14px;
        }

        .fs-16 {
            font-size: 16px;
        }

        .form-check {
            display: block;
            min-height: 1.5rem;
            padding-left: 1.5em;
            margin-bottom: 0.125rem;
        }

        .Bankcards {
            flex-direction: row;
        }

        .Bankcards img {
            width: 30%;
        }

        .cards-wrapper {
            display: flex;
        }

        .voucher {
            border-radius: 0px 40px 0 40px;
        }

        .InnerDiv {
            border-radius: 0px 40px 0 40px;
        }
        .carousel-indicators li{
            width: 15px;
            height: 15px;
            border-radius:100%;
        }
        .card-footer{
       
       padding: 0.2rem 1.25rem 0.75rem;
   }
   
      

      

       
       

       

      

      

      

        

      

       

        
    </style>
    

      </style>

</head>

<body>

    
    <div>
        <nav class="navbar navbar-expand-lg " style="background-color:#dcd2d0; color:#fff;">
            <div class="col-12 col-md-3 col-lg-3 text-center">
                <a class="navbar-brand" href="#"><img src="<?php echo base_url() ?>/assets/img/logo-header.png" class="w-100 " alt=""></a>
            </div>
            <!-- <a class="navbar-brand" href="#">Gifthos</a> -->
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            
            <form class="form-inline my-2 my-lg-0 col-12 col-md-4 col-lg-4">
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for products">
                    <div class="input-group-append">
                        <span class="input-group-text " style="background-color:#5e3a26" >
                            <i class="fas fa-" style="color:#fff">Search</i>
                        </span>                   
                    </div>
                </div>
            </form>

            <div class="collapse navbar-collapse" >
                <ul class="navbar-nav ml-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="#">Offer </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Corporate gifting</a>
                </li>
              <!--   <li class="nav-link" style="margin-top:-3px;"> 
                    <i class="fa fa-user" aria-hidden="true"></i>
                </li> -->
                     <li class="nav-item" style="margin-top:7px;">
                     <i class="fas fa-user" aria-hidden="true">&nbsp;&nbsp;</i><a href="" class="nav-item">Login/Signup </a>
                </li>
                
                <li class="nav-link"> 
                    <i class="fas fa-shopping-cart" style="color:#5e3a26"></i></a>
                </li>
                </ul>
            </div>

            <!-- <div class="collapse navbar-collape d-none" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="#">Offer </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Corporate gifting</a>
                </li>
                <li class="nav-link"> 
                    <i class="fa fa-user" aria-hidden="true"></i>
                    <a href="" class="nav-item" >Login/Signup </a>
                </li>
                <li class="nav-link"> 
                    <i class="fas fa-shopping-cart " style="color:#fff"></i></a>
                </li>
                </ul>
            </div> -->
        </nav>
        <div class="row">
           <div class="col-3 col-md-3 col-lg-3 p-0 "><img src="<?php echo base_url() ?>/assets/img/cc.png" alt="" width="300"></div> 
            <div class="col-8 col-md-8 col-lg-8">
                <div class="row border-top">                    
                    <div>
                        <nav class="navbar navbar-expand-lg bg-light navbar-light py-3 py-lg-0 px-0" style="margin-top:5px;">
                            <!-- <a href="" class="text-decoration-none d-block d-lg-none">
                                <h1 class="m-0 display-5 font-weight-semi-bold"><span class="text-primary font-weight-bold border px-3 mr-1">E</span>Shopper</h1>
                            </a> -->
                            <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbarCollapse">
                                <span class="navbar-toggler-icon"></span>
                            </button>
                            <div class="collapse navbar-collapse justify-content-between" id="navbarCollapse">
                                <div class="navbar-nav mr-auto py-0">


                                    
                                    <a href="index.html" class="nav-item nav-link active"><img src="<?php echo base_url() ?>/assets/img/homee.png">Home</a>
                                    <a href="shop.html" class="nav-item nav-link"><img src="<?php echo base_url() ?>/assets/img/shop1.png">Shop</a>

                                    
                                    <a href="contact.html" class="nav-item nav-link"><img src="<?php echo base_url() ?>/assets/img/dining1.png">Dining</a>
                                    <a href="contact.html" class="nav-item nav-link"><img src="<?php echo base_url() ?>/assets/img/electronics1.png">Electronics</a>
                                    <a href="contact.html" class="nav-item nav-link"><img src="<?php echo base_url() ?>/assets/img/fashion1.png">Fashion</a>
                                    <a href="contact.html" class="nav-item nav-link"><img src="<?php echo base_url() ?>/assets/img/wellness1.png">Wellness</a>
                                    <a href="contact.html" class="nav-item nav-link"><img src="<?php echo base_url() ?>/assets/img/gifting1.png">Gifting</a>
                                    <a href="contact.html" class="nav-item nav-link"><img src="<?php echo base_url() ?>/assets/img/travel1.png">Travel</a>
                                    <a href="contact.html" class="nav-item nav-link"><img src="<?php echo base_url() ?>/assets/img/entertainment1.png">Entertainment</a>
                                
                                </div>
                                
                        </div></nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
   
</span>
</h1>
</a>
</div>
    <!-- Topbar End -->                  

    <!-- Navbar Start -->
    <!-- <div class="container-fluid mb-2" style="margin-top: -19px;background-color: #76220a;">
        <div class="row border-top">
            
            <div class="col-lg-14" style="padding-left: 412px;">
                <div class="row border-top">
                    
                    <div class="col-lg-14" style="padding-left: 412px;">
                        <nav class="navbar navbar-expand-lg bg-light navbar-light py-3 py-lg-0 px-0">
                             <a href="" class="text-decoration-none d-block d-lg-none">
                                <h1 class="m-0 display-5 font-weight-semi-bold"><span class="text-primary font-weight-bold border px-3 mr-1">E</span>Shopper</h1>
                            </a> -->
                            <button type="button" class="navbar-toggler d-none" data-toggle="collapse" data-target="#navbarCollapse">
                                <span class="navbar-toggler-icon"></span>
                            </button>
                            <div class="collapse navbar-collapse justify-content-between" id="navbarCollapse">
                                <div class="navbar-nav mr-auto py-0">


                                    
                                    <a href="index.html" class="nav-item nav-link active" style="padding:20px"><i class="fa fa-gift" aria-hidden="true"></i><br>Home</a>
                                    <a href="shop.html" class="nav-item nav-link" style="padding:20px"><i class="fa fa-gift" aria-hidden="true"></i><br>Shop</a>

                                    
                                    <a href="contact.html" class="nav-item nav-link" style="padding:20px"><i class="fa fa-gift" aria-hidden="true"></i><br>Dining</a>
                                    <a href="contact.html" class="nav-item nav-link" style="padding:20px"><i class="fa fa-gift" aria-hidden="true"></i><br>Electronics</a>
                                    <a href="contact.html" class="nav-item nav-link" style="padding:20px"><i class="fa fa-gift" aria-hidden="true"></i><br>Fashion</a>
                                    <a href="contact.html" class="nav-item nav-link" style="padding:20px"><i class="fa fa-gift" aria-hidden="true"></i><br>Wellness</a>
                                    <a href="contact.html" class="nav-item nav-link" style="padding:20px"><i class="fa fa-gift" aria-hidden="true"></i><br>Gifting</a>
                                    <a href="contact.html" class="nav-item nav-link" style="padding:20px"><i class="fa fa-gift" aria-hidden="true"></i><br>Travel</a>
                                    <a href="contact.html" class="nav-item nav-link" style="padding:20px"><i class="fa fa-gift" aria-hidden="true"></i><br>Entertainment</a>
                                
                                </div>
                                
                        </div></nav>
                    </div>
                </div>
            </div>
        </div>
    </div> 


    <!-- <slider> -->
      
    <!-- Indicators -->
   
              
        </div>
        <br>



    </div>
   
    <!-- Navbar End -->