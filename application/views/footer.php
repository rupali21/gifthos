
    <!-- Footer Start -->
    <div class="container-fluid bg-secondary text-dark mt-5" style="background-color:#dcd2d0 !important;">
        <div class="row px-xl-5 pt-5">
            <div class="col-lg-4 col-md-12 mb-5 pr-3 pr-xl-5">
                <a href="" class="text-decoration-none">
                    <h1 class="mb-4 display-5 font-weight-semi-bold"><img src="<?php echo base_url() ?>/assets/img/header-logo.png"></h1>
                </a>
               <div class="row-footer">
               <!-- Facebook -->
           <a href="#"><i class="fa fa-facebook-f " style="color: #3b5998;"></i></a>
       

                   <!-- Twitter -->
           <a href="#"> <i class="fa fa-twitter" style="color: #55acee;"></i></a>
          <!-- Instagram -->
               <a href="#"><i class="fa fa-instagram " style="color: #ac2bac;"></i></a>

                     <!-- Linkedin -->
                        <a href="#"><i class="fa fa-linkedin-in " style="color: #0082ca;"></i></a>

            </div>
        </div>
            <div class="col-lg-8 col-md-12">
                <div class="row">
                    <div class="col-md-4 mb-5">
                        <h5 class=" text-dark ">Get in touch with us for the best strategic solutions</h5>
                       
                    </div>
                    <div class="col-md-4 mb-5">
                        <h5 class="font-weight-bold text-dark mb-4">Quick Links</h5>
                        <div class="d-flex flex-column justify-content-start">
                            <a class="text-dark mb-2" href="index.html">Home</a>
                            <a class="text-dark mb-2" href="shop.html">Our Shop</a>
                           
                        </div>
                    </div>
                    <div class="col-md-4 mb-5">
                        <h5 class="font-weight-bold text-dark mb-4">Important Links</h5>
                        <div class="d-flex flex-column justify-content-start">
                            <a class="text-dark mb-2" href="index.html">Home</a>
                            <a class="text-dark mb-2" href="shop.html">Our Shop</a>
                            <a class="text-dark mb-2" href="<?php echo site_url(); ?>/ContactPage/policy">Privacy Policy</a>
                            <a class="text-dark mb-2" href="<?php echo site_url(); ?>/ContactPage/faq">FAQs</a>

                           
                        </div>
                    </div>

                    
                </div>
            </div>
        </div>
        <div class="row border-top border-light mx-xl-5 py-4">
            <div class="col-md-6 px-xl-0" style="margin-left: 333px;">
                <p class="mb-md-0 text-center text-md-left text-dark">
                    &copy; <a class="text-dark font-weight-semi-bold" href="#">Gifthos</a>. All Rights Reserved. Designed
                    by<a href="https://www.marketingintelligence.tech/">MI-Tech Solution </a>
                  
                </p>
            </div>
            <div class="col-md-6 px-xl-0 text-center text-md-right">
                <img class="img-fluid" src="img/payments.png" alt="">
            </div>
        </div>
    </div>
    <!-- Footer End -->
    <!-- Back to Top -->
    <a href="#" class="btn btn-primary back-to-top"><i class="fa fa-angle-double-up"></i></a>


    <!-- JavaScript Libraries -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js"></script>
    
    <script src="<?php echo base_url() ?>/assets/lib/easing/easing.min.js"></script>
    <script src="<?php echo base_url() ?>/assets/lib/owlcarousel/owl.carousel.min.js"></script>

    <!-- Contact Javascript File -->
    <script src="<?php echo base_url() ?>/assets/mail/jqBootstrapValidation.min.js"></script>
    <script src="<?php echo base_url() ?>/assets/mail/contact.js"></script>

    <!-- Template Javascript -->
    <script src="<?php echo base_url() ?>/assets/js/main.js"></script>
    <script src="js/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/infiniteslidev2@2.0.1-1/infiniteslidev2.min.js"></script>
  <script src="<?php echo base_url() ?>/assets/js/Infinite/infiniteslide.js"></script>
  <script>
    $(document).ready(function() {
      $('.scroll7').infiniteslide({
        direction: 'right',
        clone: 10
      });
    });

  </script>
    
    
</body>

</html>