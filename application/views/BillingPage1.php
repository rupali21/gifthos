<!DOCTYPE html>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>EShopper - Bootstrap Shop Template</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="Free HTML Templates" name="keywords">
    <meta content="Free HTML Templates" name="description">
    <title>Billing Page</title>

    <!-- Favicon -->
    <link href="img/favicon.ico" rel="icon">

    <!-- Bootstrap cdn links -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <!-- Google Web Fonts -->
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">

    <!-- Font Awesome -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.0/css/all.min.css" rel="stylesheet">

    <!-- Libraries Stylesheet -->
    <link href="<?php echo base_url() ?>assets/lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">

    <!-- Customized Bootstrap Stylesheet -->
    <link href="<?php echo base_url() ?>assets/css/style.css" rel="stylesheet" />

    <link href="<?php echo base_url() ?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />

    <!-- Links for voucher page  -->

    <!-- <link src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css">

    </link> -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous" />

    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" />
    <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css" rel="stylesheet" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/fontawesome.min.css" rel="stylesheet"/> -->

    <!-- <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@fortawesome/fontawesome-free@5.15.4/css/fontawesome.min.css" integrity="sha384-jLKHWM3JRmfMU0A5x5AkjWkw/EYfGUAGagvnfryNV3F9VqM98XiIH7VBGVoxVSc7" crossorigin="anonymous"c/> -->


    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script> -->
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script> -->

    <!-- <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.bundle.min.js"></script> -->

    <!-- <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script> -->

    <!-- Links for modal -->
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script> -->
    <style>
        .fs-12 {
            font-size: 12px;
        }

        .fs-18 {
            font-size: 18px;
        }

        .cartGrid .qtyBox {
            display: inline-block;
            max-width: 60px;
        }

        /* .qtyBox {
    width: 100%;
    position: relative;
} */
        .cartGrid .qtyBox input {
            height: 22px;
            font-weight: 400;
            font-size: 14px;
        }

        .qtyBox input {
            width: 100%;
            height: 35px;
            border: 0;
            pointer-events: none;
            background: transparent;
            font-weight: 500;
            font-size: 16px;
            color: #6C757D;
        }

        span {
            cursor: pointer;
        }



        .minus,
        .plus {
            width: 20px;
            height: 20px;
            background: #fff;
            border-radius: 4px;
            padding: 8px 5px 8px 5px;

            vertical-align: middle;
            text-align: center;
        }



        input {
            height: 30px;
            width: 20px;
            text-align: center;
            font-size: 20px;
            border: 1px solid #fff;
            border-radius: 4px;
            display: inline-block;
            vertical-align: middle;
        }

        .custom-control {
            position: relative;
            display: block;
            min-height: 2.5rem;
        }

        .payOption:hover {

            background-color: grey;

        }
    </style>


</head>

<body>

    <div class="container-fluid m-4">


        <section style="background-color: #eee">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-md-8">

                        <div class="border-md-bottom p-3 bg-white mb-3 mb-md-0">
                            <div class="border-bottom" style="border-bottom: 2px solid #5d3a26!important;">
                                <div class="row m-0 align-items-center">

                                    <p class="text-center h3">
                                        <i class="fa fa-user fa-2x mr-2" aria-hidden="true"></i>User Details
                                    </p>

                                    <div class="col-lg-11 col-12 px-0 px-lg-3">
                                        <div class="pb-2 py-lg-2 deliver-details">
                                            <div class="d-lg-flex align-items-lg-center justify-content-between">
                                                <ul class="list-group list-group-flush">
                                                    <li class="list-group-item"><span class="text-black h6">Voucher billed to :</span> Geeta Ramchandra Paralikar</li>
                                                    <li class="list-group-item"><span class="text-black"><i class="fa fa-phone" aria-hidden="true"></i>
                                                        </span>9010810108 </li>
                                                    <li class=" list-group-item"><span class="text-black"><i class="fa fa-envelope" aria-hidden="true"></i>
                                                        </span>geeta@gmail.com </li>

                                                </ul>
                                            </div>
                                        </div>
                                        <div class="pb-2 py-lg-2 deliver-details">
                                            <div class="d-lg-flex align-items-lg-center justify-content-between">
                                                <ul class="list-inline">
                                                    <li class="list-inline-item"><span class="text-black h6">Will be delivered via :</span>

                                                    </li>
                                                    <li class="list-inline-item">

                                                        <label class="form-check-label" for="exampleCheck2"><i class="fas fa-sms"></i>
                                                            Sms and Email mentioned above</label>

                                                    </li>
                                                    <li class=" list-inline-item"><span class="text-black">
                                                        </span> </li>

                                                </ul>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>


                        <!-- code for 2nd voucher -->

                        <div class="row mx-0 border-bottom py-3 bg-white cartGrid">
                            <div class="col-lg-2 pb-0 d-none d-lg-block">
                                <div class="border d-flex w-100 h-100 align-items-center text-center">
                                    <a href="">
                                        <div class="w-100"><img src="<?php echo base_url() ?>assets/img/electronics.jpg" width="100" height="100"></div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-lg-10 col-12">
                                <div class="row justify-content-between pb-3">
                                    <div class="col-6 col-md-auto lh-1 fs-18 fw-500">
                                        C World
                                    </div>
                                    <div class="col-md-auto lh-1 ps-0">
                                        <div class="text-nowrap text-end">
                                            <div class="align-items-center justify-content-end">
                                                <div class="pe-2 pe-md-3 no-flex d-flex align-items-center">


                                                    <div class=" d-inline align-items-center">
                                                        <span class="text-black pe-1 fs-14 font-weight-bold">You Pay :</span>
                                                        <span class="fs-16 fw-500 font-weight-bold">Rs. 950</span>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 fs-12 pt-1 text-muted">E Gift Card(Instant Vouchers)</div>
                                </div>
                                <div class="row mb-1">
                                    <div class=" col-3 text-success pb-3 fs-14">
                                        9 % Off
                                    </div>
                                    <div class=" col-3 SavingAmount">
                                        <small class="text-black pe-1 fs-14">Savings</small>
                                        <span class="fs-16 fw-500 font-weight-bold">Rs. 50</span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-5 mb-3 offset-7 ">

                                        <div class="form-check">
                                            <input type="checkbox" class="form-check-input" id="exampleCheck1" data-toggle="modal" data-target=".bd-example-modal-lg">
                                            <label class="form-check-label" for="exampleCheck1"><i class="fas fa-gift mr-2"></i>I am Gifting</label>
                                        </div>

                                    </div>
                                </div>
                                <div class="row justify-content-between align-items-end">
                                    <div class="col-md-auto col-12">
                                        <ul class="col-md-auto col-12">
                                            <li class="list-inline-item mr-5">
                                                <p class="text-black fs-14 mb-0 font-weight-bold">
                                                    Value
                                                </p>
                                                <p class="fs-14 m-0">
                                                    Rs.1000
                                                </p>
                                            </li>
                                            <li class="list-inline-item mr-5">
                                                <p class="text-black fs-14 mb-0 font-weight-bold">
                                                    Quantity
                                                </p>

                                                <!-- Add & Minus buttons -->
                                                <div class="number">
                                                    <span class="minus">-</span>
                                                    <input type="text" value="1" />
                                                    <span class="plus">+</span>
                                                </div>


                                            </li>
                                            <li class="list-inline-item mr-5">
                                                <p class="text-black fs-14 mb-0 font-weight-bold">
                                                    Total Value
                                                </p>
                                                <p class="fs-14 m-0">
                                                    Rs.1000
                                                </p>
                                            </li>

                                        </ul>
                                    </div>

                                    <!-- Code  for modal  -->
                                    <div class="row">

                                        <!-- Large modal -->
                                        <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                            <div class="modal-dialog modal-lg">
                                                <div class="modal-content">
                                                    ...
                                                </div>
                                            </div>
                                        </div>
                                    </div>




                                    <!--  End of Code  for modal  -->
                                    <div class="col-md-auto text-end col-12 ps-0 pt-2">
                                        <button class="text-danger fs-14 btn p-0 border-0"><i class="fa fa-trash" aria-hidden="true"></i>
                                            Remove</button>
                                    </div>

                                </div>




                            </div>
                        </div>
                        <div class="row m-0">
                            <div class="col-12 p-0">

                            </div>
                        </div>

                    </div>


                    <!-- Cart section -->
                    <div class="col-12 col-md-4 pt-3 pt-md-0">
                        <div class="bg-white shadow-sm">
                            <div class="row m-0">
                                <div class="col-12 py-2 bg-dark fs-20 fw-400 text-white text-center"><span><i class="fa fa-shopping-cart fa-align-right" aria-hidden="true" style="color:white; float:right;"></i>
                                    </span>
                                    CART
                                </div>
                            </div>
                            <div class="row py-3 bg-light mx-0 my-2">
                                <div class="col-8 fw-500">
                                    Voucher Value
                                </div>
                                <div class="col-4 fw-500 text-end">Rs. 1000</div>
                            </div>
                            <div class="row py-3 mx-0 my-2 text-success fw-500 bg-light-green">
                                <div class="col-8 ">
                                    Savings
                                </div>
                                <div class="col-4 text-end">
                                    Rs. 50
                                </div>
                            </div>
                            <div class="row py-3 bg-light mx-0 my-2">
                                <div class="col-8 fw-500">
                                    You Pay
                                </div>
                                <div class="col-4 fw-500 text-end text-nowrap">
                                    Rs. 950
                                </div>
                            </div>
                            <div class="paymentOptions">
                                <div class="row m-0 fs-12  d-flex justify-content-between">
                                    <div class="col-5">

                                        Payment Methods
                                    </div>
                                    <div class="col-4 ">
                                        Payable <span><i class="fa fa-inr" style="box-shadow: 0 0 0px #888;"></i></span>
                                    </div>
                                    <div class="col-3 text-end">
                                        Rs. 950
                                    </div>

                                </div>


                            </div>
                            <!-- Code of radio buttons for payment options -->
                            <!-- Group of default radios - option 1 -->
                            <div class="d-block mb-2 ">
                                <div class="bg-light align-items-center m-2">
                                    <div class="payOption hoverable">
                                        <!-- <input type="radio" class="custom-control-input" id="defaultGroupExample1" name="groupOfDefaultRadios"> -->
                                        <label class="payOption-label mt-2" for="defaultGroupExample1"> <span class="pe-2">
                                                <img src="<?php echo base_url(); ?>assets/img/credit_card.jpg" height="20px">
                                            </span>Credit Card</label>
                                    </div>
                                </div>
                                <!-- Group of default radios - option 2 -->
                                <div class="bg-light align-items-center m-2">
                                    <div class="payOption">
                                        <!-- <input type="radio" class="custom-control-input" id="defaultGroupExample2" name="groupOfDefaultRadios"> -->
                                        <label class="payOption-label mt-2" for="defaultGroupExample2"><span class="pe-2">
                                                <img src="<?php echo base_url(); ?>assets/img/debit_card.png" height="20px" width="20px">
                                            </span>Debit Card</label>
                                    </div>
                                </div>
                                <!-- Group of default radios - option 3 -->
                                <div class="bg-light align-items-center m-2">
                                    <div class="align-items-center lh-1">


                                        <div class="payOption">
                                            <!-- <input type="radio" class="custom-control-input" id="defaultGroupExample3" name="groupOfDefaultRadios"> -->
                                            <label class="payOption-label mt-2" for="defaultGroupExample3"><span class="pe-2">
                                                    <img src="<?php echo base_url(); ?>assets/img/net_banking.jpg" height="20px">
                                                </span>Net Banking </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="bg-light align-items-center m-2">
                                    <div class="payOption">

                                        <!-- <input type="radio" class="custom-control-input" id="defaultGroupExample4" name="groupOfDefaultRadios"> -->

                                        <label class="payOption-label mt-2" for="defaultGroupExample4"><span class="pe-2">
                                                <img src="<?php echo base_url(); ?>assets/img/logo_upi.png">
                                            </span>UPI </label>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
    </div>
    </div>

    </div>

    <!-- selected vouchers -->



    </div>
    </div>


    </div>
    </div>
    </div>
    </section>


    <!-- Javascript for add and subtract buttons -->




    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

    <script>
        $(document).ready(function() {
            $('.minus').click(function() {
                var $input = $(this).parent().find('input');
                var count = parseInt($input.val()) - 1;
                count = count < 1 ? 1 : count;
                $input.val(count);
                $input.change();
                return false;
            });
            $('.plus').click(function() {
                var $input = $(this).parent().find('input');
                $input.val(parseInt($input.val()) + 1);
                $input.change();
                return false;
            });
        });
    </script>
    <script>
        $('#modalDialog').modal('show')
    </script>

</body>

</html>