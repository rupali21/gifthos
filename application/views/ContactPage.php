<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Gifthos</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="Free HTML Templates" name="keywords">
    <meta content="Free HTML Templates" name="description">

    <!-- Favicon -->
    <link href="<?php echo base_url() ?>assets/img/favicon.ico" rel="icon">

    <!-- Google Web Fonts -->
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">




    <!-- Libraries Stylesheet -->
    <link href="<?php echo base_url() ?>assets/lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">

    <!-- Customized Bootstrap Stylesheet -->
    <link href="<?php echo base_url() ?>assets/css/style.css" rel="stylesheet">

    <link href="<?php echo base_url() ?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />



    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.css" />

     
          <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">

   
    <style>
        .homeicon {
 
    color: #5f3825;
        }


    </style>
    </head>



  
  


    <section class="py-3 bg-white">
        <div class="container-fluid">
        <div class="row">
        <div class="col-12 col-md-12 col-lg-12 ">
      
           
           <a href="<?php echo base_url();?>"><i class="fa fa-home fa-2x homeicon ml-5" aria-hidden="true"></i></a>
    
            <img src="<?php echo base_url(); ?>assets/img/logo-header1.png" class="img-responsive center-block pb-5 mb-3" alt="Logo Image">
        </div>
       
    </div>
            <div class="row">
                <div class="col-lg-6 leftTicket">
                    <div class="row mx-0 border-bottom pb-2 mb-2">
                        <div class="col-md-4 pl-0 pr-0 pr-md-3">
                            <div class="ticketBg text-center py-3 py-md-5">
                                <img src="<?php echo base_url(); ?>assets/img/voucherStatus1.png" height="100px">
                            </div>

                        </div>
                        <div class="col-md-8 align-self-center px-4 px-md-0 mt-4 mt-md-0 text-center text-md-left">
                            <h6 class="text-uppercase">I want to check my Voucher status</h6>
                            <div class="form-group row mx-0 mb-0">
                                <div class="col-md-8 px-0">
                                    <input type="text" class="form-control rounded" name="voucherNum" maxlength="30">
                                </div>
                                <div class="col-md-4 mt-4 mt-md-0">
                                    <button class="btn btn-outline-primary rounded text-nowrap">Check Status
                                </div>
                            </div>

                        </div>
                    </div>
                    <!-- <div class="row mx-0 border-bottom pb-2 mb-2">

                    </div> -->
                </div>

                <div class="col-lg-6 rightTicket">
                    <div class="row mx-0 border-bottom pb-2 mb-2">
                        <div class="col-md-4 pl-0 pr-0 pr-md-3">
                            <div class="ticketBg text-center py-3 py-md-5">
                                <img src="<?php echo base_url(); ?>assets/img/voucherImage1.png" height="100px">
                            </div>

                        </div>
                        <div class="col-md-8 align-self-center px-4 px-md-0 mt-4 mt-md-0 text-center text-md-left">
                            <h6 class="text-uppercase">I want to resend my Voucher </h6>
                            <div class="form-group row mx-0 mb-0">
                                <div class="col-md-8 px-0">
                                    <input type="text" class="form-control rounded" name="voucherNum" maxlength="30">
                                </div>
                                <div class="col-md-4 mt-4 mt-md-0">
                                    <button class="btn btn-outline-primary rounded text-nowrap">Resend
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
           

            </div>
        </div>

        <!-- 2nd row -->
        <div class="row">
            <div class="col-6 offset-3">
                <div class="row mx-0 border-bottom pb-2 mb-2">
                    <div class="col-md-4 pl-0 pr-0 pr-md-3">
                        <div class="ticketBg text-center py-3 py-md-5">
                            <img src="<?php echo base_url(); ?>assets/img/ticket.png" height="100px">
                        </div>

                    </div>
                    <div class="col-md-8 align-self-center px-4 px-md-0 mt-4 mt-md-0 text-center text-md-left">
                        <h6 class="text-uppercase">I want to raise a ticket </h6>
                        <div class="form-group row mx-0 mb-0">
                            <div class="col-md-8 px-0">
                                <input type="text" class="form-control rounded" name="voucherNum" maxlength="30">
                            </div>
                            <div class="col-md-4 mt-4 mt-md-0">
                                <button class="btn btn-outline-primary rounded text-nowrap" data-toggle="modal" data-target="#exampleModal">Raise a Ticket</button>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>

        <!-- Code  for modal  -->
        <div class="row">

            <!-- Modal -->
          

            <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script> -->
            <!-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo">Test Modal</button> -->

            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">New message</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form>
                                <div class="form-group">
                                    <label for="recipient-name" class="col-form-label">Ticket for:</label>
                                    <select id="inputState" class="form-control">
                                        <option value=" Voucher Purchase">Voucher Purchase </option>
                                        <option value=" Voucher Redemption">Voucher Redemption</option>
                                        <option value="Payment">Payment   </option>
                                        <option value="Resend">Resend</option>
                                        <option value="Coins">Coins</option>
                                        <option value="Accounting">Accounting</option>
                                        <option value="Display">Display</option>
                                        <option value="Display">Other</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="message-text" class="col-form-label">Message:</label>
                                    <!-- <input type="textarea" class="form-control" id="message-text"> -->
                                    <textarea class="form-control" id="message-text"></textarea>

                                </div>
                                <!-- <div class="modal-footer"> -->
                                <div class="text-center">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    <button type="button" class="btn btn-primary">Submit</button>
                                </div>
                                <!-- </div> -->
                        </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script> 
    </section>
    </body>