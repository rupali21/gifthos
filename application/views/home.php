  <div id="header-carousel" class="carousel slide" data-ride="carousel" style="padding-top: 10px;">
                    <div  class="carousel-inner">
                         <ol class="carousel-indicators" >
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
      <li data-target="#myCarousel" data-slide-to="2"></li>
    </ol>

                        <div class="carousel-item active" style="height: 350px;">
                            <img class="img-fluid" src="<?php echo base_url() ?>/assets/img/carousel-1.jpg" alt="Image">
                            <div class="carousel-caption d-flex flex-column align-items-center justify-content-center">
                                <div class="p-3" style="max-width: 2000px;">
                                    <h4 class="text-light text-uppercase font-weight-medium mb-3">10% Off Your First Order</h4>
                                    <h3 class="display-4 text-white font-weight-semi-bold mb-4">Fashionable Dress</h3>
                                    <a href="" class="btn btn-light py-2 px-3">Shop Now</a>
                                </div>
                            </div>
                        </div>
                        <div class="carousel-item" style="height: 350px;">
                            <img class="img-fluid" src="<?php echo base_url() ?>/assets/img/carousel-2.jpg" alt="Image">
                            <div class="carousel-caption d-flex flex-column align-items-center justify-content-center">
                                <div class="p-3" style="max-width: 2000px;">
                                    <h4 class="text-light text-uppercase font-weight-medium mb-3">10% Off Your First Order</h4>
                                    <h3 class="display-4 text-white font-weight-semi-bold mb-4">Reasonable Price</h3>
                                    <a href="" class="btn btn-light py-2 px-3">Shop Now</a>
                                </div>
                            </div>
                        </div>
                    </div>
                     <a class="carousel-control-prev" href="#header-carousel" data-slide="prev">
                        <div class="btn btn-dark" style="width: 45px; height: 45px;">
                            <span class="carousel-control-prev-icon mb-n2"></span>
                        </div>
                    </a>
                    <a class="carousel-control-next" href="#header-carousel" data-slide="next">
                        <div class="btn btn-dark" style="width: 45px; height: 45px;">
                            <span class="carousel-control-next-icon mb-n2"></span>
                        </div>
                    </a>
                  
                
            </div>
            <!-- <end slider> -->

<!-- Products Start -->
    <div class="container-fluid pt-5" style="background-color:#dcd2d0">
        <div class="text-center mb-4">
            <h2 class="section-title"><span style="background-color:#dcd2d0">Trending Gift Cards</span></h2>
        </div>

        <div class="row px-xl-5 pb-3">
            <div class="col-lg-3 col-md-6 col-sm-12 pb-1">
                <div class="card1 product-item border-1 mb-4">
                    <a href="<?php echo site_url() ?>/voucher"> <div class="card-header product-img position-relative overflow-hidden bg-transparent border p-0">
                       <img class="img-fluid w-100" src="assets/img/cards2.png" alt="">
                         <div class="bottom-left-offer"><img href="#" width="65" height="55" src="assets/img/newone.png">
                            
                            </div></a>
                            
                        </div>
                         <div class="card-body text-center p-0  border border-bottom-0">
                        <h6 class="text-truncate mb-3  pt-2">Gift card</h6>
                        <div class="d-flex justify-content-center">
                            
                        </div>
                    </div>
                
                    <div class="card-footer d-flex justify-content-between  border border-top-0">
                        <a href="" class="btn btn-sm text-dark p-0"><i class="fas fa-eye text-primary mr-1"></i>View Detail</a>
                        <a href="" class="btn btn-sm text-dark p-0"><i class="fas fa-shopping-cart text-primary mr-1"></i>Add To Cart</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-12 pb-1">
                <div class="card1 product-item border-1 mb-4">
                    <a href="<?php echo site_url() ?>/voucher"> <div class="card-header product-img position-relative overflow-hidden  bg-transparent border p-0">
                        <img class="img-fluid w-100" src="assets/img/cards3.png" alt="">
                            <div class="bottom-left-offer"><a href="#"><img href="#" width="65" height="55" src="assets/img/newone.png" ></a>
                            
                            </div>
                    </div></a>
                    <div class="card-body  text-center p-0 border border-bottom-0">
                        <h6 class="text-truncate mb-3  pt-2">Quickcliver</h6>
                        <div class="d-flex justify-content-center">
                            
                        </div>
                    </div>
                    <div class="card-footer d-flex justify-content-between  border border-top-0">
                        <a href="" class="btn btn-sm text-dark p-0"><i class="fas fa-eye text-primary mr-1"></i>View Detail</a>
                        <a href="" class="btn btn-sm text-dark p-0"><i class="fas fa-shopping-cart text-primary mr-1"></i>Add To Cart</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-12 pb-1">
                <div class="card1 product-item border-1 mb-4">
                    <a href="<?php echo site_url() ?>/voucher"> <div class="card-header product-img position-relative overflow-hidden  bg-transparent border p-0">
                        <img class="img-fluid w-100" src="assets/img/cards6.png" alt="">
                            <div class="bottom-left-offer"><a href="#"><img href="#" width="65" height="55" src="assets/img/newone.png" ></a>
                            
                            </div>
                    </div></a>
                    <div class="card-body  text-center p-0  border border-bottom-0">
                        <h6 class="text-truncate mb-3  pt-2 ">Gift card</h6>
                        <div class="d-flex justify-content-center">
                            
                        </div>
                    </div>
                    <div class="card-footer d-flex justify-content-between border border-top-0">
                        <a href="" class="btn btn-sm text-dark p-0"><i class="fas fa-eye text-primary mr-1"></i>View Detail</a>
                        <a href="" class="btn btn-sm text-dark p-0"><i class="fas fa-shopping-cart text-primary mr-1"></i>Add To Cart</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-12 pb-1">
                <div class="card1 product-item border-1 mb-4">
                     <a href="<?php echo site_url() ?>/voucher"><div class="card-header product-img position-relative overflow-hidden  bg-transparent border p-0">
                        <img class="img-fluid w-100" src="assets/img/cards5.png" alt="">
                            <div class="bottom-left-offer"><a href="#"><img href="#" width="65" height="55" src="assets/img/newone.png" ></a>
                            
                            </div>
                    </div></a>
                    <div class="card-body  text-center p-0 border border-bottom-0">
                        <h6 class="text-truncate mb-3  pt-2">Myntra</h6>
                        <div class="d-flex justify-content-center">
                            
                        </div>
                    </div>
                    <div class="card-footer d-flex justify-content-between  border border-top-0">
                        <a href="" class="btn btn-sm text-dark p-0"><i class="fas fa-eye text-primary mr-1"></i>View Detail</a>
                        <a href="" class="btn btn-sm text-dark p-0"><i class="fas fa-shopping-cart text-primary mr-1"></i>Add To Cart</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-12 pb-1">
                <div class="card1 product-item border-0 mb-4">
                    <a href="<?php echo site_url() ?>/voucher"> <div class="card-header product-img position-relative overflow-hidden  bg-transparent border p-0">
                        <img class="img-fluid w-100" src="assets/img/cards4.png" alt="">
                            <div class="bottom-left-offer"><a href="#"><img href="#" width="65" height="55" src="assets/img/newone.png" ></a>
                            
                            </div>
                    </div></a>
                    <div class="card-body  text-center p-0  border border-bottom-0">
                        <h6 class="text-truncate mb-3  pt-2">lifestyle</h6>
                        <div class="d-flex justify-content-center">
                            
                        </div>
                    </div>
                    <div class="card-footer d-flex justify-content-between  border border-top-0">
                        <a href="" class="btn btn-sm text-dark p-0"><i class="fas fa-eye text-primary mr-1"></i>View Detail</a>
                        <a href="" class="btn btn-sm text-dark p-0"><i class="fas fa-shopping-cart text-primary mr-1"></i>Add To Cart</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-12 pb-1">
                <div class="card1 product-item border-0 mb-4">
                     <a href="<?php echo site_url() ?>/voucher"><div class="card-header product-img position-relative overflow-hidden  bg-transparent border p-0">
                        <img class="img-fluid w-100" src="assets/img/pink.png" alt="">
                            <div class="bottom-left-offer"><a href="#"><img href="#" width="65" height="55" src="assets/img/newone.png" ></a>
                            
                            </div>
                    </div></a>
                    <div class="card-body  text-center p-0 border border-bottom-0">
                        <h6 class="text-truncate mb-3  pt-2">Estside</h6>
                        <div class="d-flex justify-content-center">
                            
                        </div>
                    </div>
                    <div class="card-footer d-flex justify-content-between border border-top-0">
                        <a href="" class="btn btn-sm text-dark p-0"><i class="fas fa-eye text-primary mr-1"></i>View Detail</a>
                        <a href="" class="btn btn-sm text-dark p-0"><i class="fas fa-shopping-cart text-primary mr-1"></i>Add To Cart</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-12 pb-1">
                <div class="card1 product-item border-0 mb-4">
                    <a href="<?php echo site_url() ?>/voucher"> <div class="card-header product-img position-relative overflow-hidden  bg-transparent border  p-0">
                        <img class="img-fluid w-100" src="assets/img/cards.jpg" alt="">
                            <div class="bottom-left-offer"><a href="#"><img href="#" width="65" height="55" src="assets/img/newone.png" ></a>
                            
                            </div>
                    </div></a>
                    <div class="card-body  text-center p-0 border border-bottom-0">
                        <h6 class="text-truncate mb-3 pt-2">Dominoz</h6>
                        <div class="d-flex justify-content-center">
                            
                        </div>
                    </div>
                    <div class="card-footer d-flex justify-content-between  border border-top-0">
                        <a href="" class="btn btn-sm text-dark p-0"><i class="fas fa-eye text-primary mr-1"></i>View Detail</a>
                        <a href="" class="btn btn-sm text-dark p-0"><i class="fas fa-shopping-cart text-primary mr-1"></i>Add To Cart</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-12 pb-1">
                <div class="card1 product-item border-0 mb-4">
                     <a href="<?php echo site_url() ?>/voucher"><div class="card-header product-img position-relative overflow-hidden  bg-transparent border p-0">
                        <img class="img-fluid w-100" src="assets/img/cards2.png" alt="">
                            <div class="bottom-left-offer"><a href="#"><img href="#" width="65" height="55" src="assets/img/newone.png" ></a>
                            
                            </div>
                    </div></a>
                    <div class="card-body text-center p-0 border border-bottom-0">
                        <h6 class="text-truncate mb-3 pt-2">vijay sales</h6>
                        <div class="d-flex justify-content-center">
                            
                        </div>
                    </div>
                    <div class="card-footer d-flex justify-content-between  border border-top-0">
                        <a href="" class="btn btn-sm text-dark p-0"><i class="fas fa-eye text-primary mr-1"></i>View Detail</a>
                        <a href="" class="btn btn-sm text-dark p-0"><i class="fas fa-shopping-cart text-primary mr-1"></i>Add To Cart</a>
                    </div>
                </div>
            </div>
           <button type="button" class="btn btn-primary btn-rounded" style="margin-left:550px";><b>View more</b></button>

        </div>
    </div>
    <!-- Products End -->


         

         



    <!-- Products Start -->
   <div class="container-fluid py-5">
        <div class="text-center mb-4">
            <h2 class="section-title px-5"><span class="px-2">Businesses growing with Gifthos</span></h2>
        </div>
        <div class="row px-xl-5">
            <div class="col">
                <div class="owl-carousel related-carousel">
                    <div class="card product-item border-0">
                        <div class="card-header product-img position-relative overflow-hidden  border p-0">
                           <img class="img-fluid w-100" src="<?php echo base_url() ?>/assets/img/222.png" alt="">
                        </div>
                       
                       
                    </div>
                    <div class="card product-item border-0">
                        <div class="card-header product-img position-relative overflow-hidden bg-secondary border p-0">
                            <img class="img-fluid w-100" src="<?php echo base_url() ?>/assets/img/333.png" alt="">
                        </div>
                       
                    </div>
                    <div class="card product-item border-0">
                        <div class="card-header product-img position-relative overflow-hidden bg-secondary border p-0">
                            <img class="img-fluid w-100" src="<?php echo base_url() ?>/assets/img/b6.png" alt="">
                        </div>
                       
                    </div>
                    <div class="card product-item border-0">
                        <div class="card-header product-img position-relative overflow-hidden bg-secondary border p-0">
                            <img class="img-fluid w-100" src="<?php echo base_url() ?>/assets/img/888.png" alt="">
                        </div>
                       
                    </div>
                    <div class="card product-item border-0">
                        <div class="card-header product-img position-relative overflow-hidden bg-transparent border p-0">
                            <img class="img-fluid w-100" src="<?php echo base_url() ?>/assets/img/999.png" alt="">
                        </div>
                       

                    </div>

                </div><br><br>

           <button type="button" class="btn btn-primary btn-rounded" style="margin-left:550px";><b>view all</b></button>

            </div>
        </div>
    </div>
    <!-- Products End -->
    <!-- Products Start -->
    <div class="container-fluid pt-5" style="background-color:#dcd2d0">
        <div class="text-center mb-4">
            <h2 class="section-title"><span style="background-color:#dcd2d0">Our Featured Products</span></h2>
        </div>

        <div class="row px-xl-5 pb-3">
            <div class="col-lg-3 col-md-6 col-sm-12 pb-1">
                <div class="card1 product-item border-0 mb-4">
                    <a href="<?php echo site_url() ?>/voucher"> <div class="card-header1 product-img position-relative overflow-hidden  p-0">
                       <img class="img-fluid w-100" src="assets/img/rr.png" alt="">
                       </div></a>
                            <div class="card-footer d-flex justify-content-between bg-light border">
                        <a href="" class="btn btn-sm text-dark p-0"><i class='fas fa-envelope'></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Enquiry</a>
                       
                    </div>

                        
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-12 pb-1">
                <div class="card1 product-item border-0 mb-4">
                    <a href="<?php echo site_url() ?>/voucher"> <div class="card-header1 product-img position-relative overflow-hidden  p-0">
                        <img class="img-fluid w-100" src="assets/img/rr1.png" alt="">
                           
                    </div></a>
                    <div class="card-footer d-flex justify-content-between bg-light border">
                        <a href="" class="btn btn-sm text-dark p-0"><i class='fas fa-envelope'></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Enquiry</a>
                       
                    </div>
                   
                </div>
            </div>

            <div class="col-lg-3 col-md-6 col-sm-12 pb-1">
                <div class="card1 product-item border-0 mb-4">
                    <a href="<?php echo site_url() ?>/voucher"> <div class="card-header1 product-img position-relative overflow-hidden  p-0">
                        <img class="img-fluid w-100" src="assets/img/rr2.png" alt="">
                            
                    </div></a>

                    <div class="card-footer d-flex justify-content-between bg-light border">
                        <a href="" class="btn btn-sm text-dark p-0"><i class='fas fa-envelope'></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Enquiry</a>
                       
                    </div>
                   
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-12 pb-1">
                <div class="card1 product-item  mb-4">
                     <a href="<?php echo site_url() ?>/voucher"><div class="card-header1 product-img position-relative overflow-hidden  p-0">
                        <img class="img-fluid w-100" src="assets/img/rr3.png" alt="">
                           
                    </div></a>

                    <div class="card-footer d-flex justify-content-between bg-light border">
                        <a href="" class="btn btn-sm text-dark p-0"><i class='fas fa-envelope'></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Enquiry</a>
                       
                    </div>
                   
                </div>
            </div>
            
          
            
           <button type="button" class="btn btn-primary btn-rounded" style="margin-left:550px";><b>view all</b></button>

        </div>
    </div>
  
    <!-- Products End -->

    <!-- <video coursole slider start> -->
<div class="text-center mb-4">
            <h2 class="section-title px-5" style="padding-top:15px";><span class="px-2">Discounts & Offers</span></h2>
            
            
    <!-- Content to be scrolled -->
    <div class="row px-xl-5 pb-3">
        <div class="scroll7">
            <div class="col-lg-3 col-md-6 col-sm-12 pb-1"style="padding-top: 74px;">
                <div class="card product-item border-0 mb-4">
                    <div class="card-header product-img position-relative overflow-hidden">
                        <img class="img-fluid w-100" src="assets/img/11.jpg" alt="">
                    </div>
                    
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-12 pb-1">
                <div class="card product-item border-0 mb-4">
                    <div class="card-header product-img position-relative overflow-hidden ">
                        <img class="img-fluid w-100" src="assets/img/12.jpg" alt="">
                    </div>
                    
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-12 pb-1"style="padding-top: 74px;">
                <div class="card product-item border-0 mb-4">
                    <div class="card-header product-img position-relative overflow-hidden">
                        <img class="img-fluid w-100" src="assets/img/13.jpg" alt="">
                    </div>
                   
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-12 pb-1">
                <div class="card product-item border-0 mb-4">
                    <div class="card-header product-img position-relative overflow-hidden">
                        <img class="img-fluid w-100" src="assets/img/14.jpg" alt="">
                    </div>
                   
                </div>
            </div>

            
            
           

        </div>


           <button type="button" class="btn btn-primary btn-rounded" style="margin-left:550px";><b>View More</b></button>


  </div>
           
            
           
                      

        </div>

    
  


<!--         <video coursole slider end>
 -->



<!-- <Event special Offers section start>-->
<!-- <slider start> -->
        <div class="container-fluid pt-5" style="background-color:#dcd2d0">
        <div class="text-center mb-4">
            <h2 class="section-title px-4"><span class="px-2" style="vertical-align: 50px; background-color:#dcd2d0 ;">Events Special </span></h2>
        </div>
        <div class="row px-xl-5 pb-3" style="margin: -62px;">
            <div class="col-lg-3 col-md-6 col-sm-12 pb-1">
                <div class="" style="width: 16rem;">
   <a href="<?php echo site_url() ?>/anniversary"><img src="assets/img/anniversary.png" class="card-img-top" alt="..." style="border-radius: 10px 50px;
"></a>
  
</div>

            </div>
            <div class="col-lg-3 col-md-6 col-sm-12 pb-1">
                <div class="" style="width: 16rem;">
  <a href="#"><img src="assets/img/freiendship.png" class="card-img-top" alt="..." style="border-radius: 10px 50px;
"></a>
  
</div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-12 pb-1">
                <div class="" style="width: 16rem;">
  <a href="#"><img src="assets/img/rakshabandhan.png" class="card-img-top" alt="..." style="border-radius: 10px 50px;
"></a>
 
</div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-12 pb-1">
                <div class="" style="width: 16rem;">
  <a href="#"><img src="assets/img/birthday.png" class="card-img-top" alt="..." style="border-radius: 10px 50px;
"></a>
  
</div><br><br><br>
            </div>
           
           
           
        </div>
    </div>
         

        <!-- <slider end> -->

    <!-- <Event special Offers section end>-->


 

  <!-- Cart Start -->
    <div class="container-fluid pt-5">
        <div class="text-center mb-4">
          <h2 class="section-title px-5" style="padding-top:25px";><span class="px-2">Our Blog</span></h2>
        </div>
        <div class="row px-xl-5">

            <div class="col-lg-6 table-responsive mb-5">
               
                    <h1>What is Lorem Ipsum?</h1>
 <p style="text-align: justify;font-size: 12px;">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.


                </p>
<button type="button" class="btn btn-primary btn-rounded"><b>View More</b></button>



                
            </div>
            <div class="col-lg-4">
                <div style="padding-left: 158px;">
                                 <iframe width="280" height="150" src="https://www.youtube.com/embed/tgbNymZ7vqY">
</iframe>
                    
                </div>

            </div>
        </div>
    </div>
    <!-- Cart End -->
    

   


  