<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Gifthos</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="Free HTML Templates" name="keywords">
    <meta content="Free HTML Templates" name="description">

    <!-- Favicon -->
    <link href="<?php echo base_url() ?>assets/img/favicon.ico" rel="icon">

    <!-- Google Web Fonts -->
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">

    <!-- Font Awesome -->
    <!-- <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.0/css/all.min.css" rel="stylesheet"> -->
    <!-- <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" /> -->


    <!-- Libraries Stylesheet -->
    <link href="<?php echo base_url() ?>assets/lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">

    <!-- Customized Bootstrap Stylesheet -->
    <link href="<?php echo base_url() ?>assets/css/style.css" rel="stylesheet">

    <link href="<?php echo base_url() ?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />



    <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"> -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.css" />


    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>  -->
    <!--  </head> -->
    <style>
        .homeicon {

            color: #5f3825;
        }
    </style>
</head>

<body>
<div class="container">

    <div class="row">
        <div class="col-12 col-md-12 col-lg-12 ">

            <a href="<?php echo base_url(); ?>"><i class="fa fa-home fa-2x homeicon ml-5" aria-hidden="true"></i></a>

            <img src="<?php echo base_url(); ?>assets/img/logo-header1.png" class="img-responsive center-block pb-5 mb-3" alt="Logo Image">

        </div>

    </div>
    <section class="textPageSection p-3">

        <div class="container px-3 fs-14">

            <h4 class="border-bottom pb-2 mb-3 text-center">Privacy Policy</h4>
            <div class="row">
                <div class="col-12 m-3">
                    <p></p>
                </div>
            </div>
         
        </div>

    </section>
</div>
</body>