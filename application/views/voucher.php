<!DOCTYPE html>
<html>

<!-- Links for voucher page  -->

<!-- <link src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css">

    </link> -->
<!-- <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous" /> -->

<!-- <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" /> -->
<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css" rel="stylesheet" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/fontawesome.min.css" rel="stylesheet"/> -->

<!-- <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@fortawesome/fontawesome-free@5.15.4/css/fontawesome.min.css" integrity="sha384-jLKHWM3JRmfMU0A5x5AkjWkw/EYfGUAGagvnfryNV3F9VqM98XiIH7VBGVoxVSc7" crossorigin="anonymous"c/> -->


<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script> -->

<!-- <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.bundle.min.js"></script> -->

<!-- <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script> -->



<head>
<style>


.card {
  
    border: 0px solid rgba(0,0,0,.125);
   
}
</style>



</head>

<body>

    <!-- Code for Brand logo -->
    <section>
        <div class="container-fluid px-5 py-1">
            <div class="row justify-content-center mb-3">
                <div class="col-md-12 col-xl-12" style=" border-radius: 25px;background-color:#dcd2d0" ;>
                    <div class="card shadow-0"style="  background-color:#dcd2d0">
                        <div class="card-body">
                            <div class="row" id="logoBanner">
                                <div class="col-md-8 col-lg-6 col-xl-8">
                                    <h1>BigBasket</h1>


                                    <div id="product-carousel" class="carousel slide pt-5" data-ride="carousel">
                                        <div class="carousel-inner" style="width: 77%; margin-left: 79px;">
                                            <div class="carousel-item active">
                                                <p class="fs-14 pt-2 pt-md-0">
                                                    Getting groceries at home is made simpler by BigBasket.
                                                    This online megastore offers farm fresh veggies, fruits, dairy products, and more to your doorstep.
                                                    Discover daily essentials and gourmet food with 20k plus options to choose from.
                                                    At Gifthos, we offer BigBasket gift cards at discounted prices for you.
                                                    Redeem your gift card easily on your online orders.</p>
                                            </div>
                                            <div class="carousel-item">
                                                <p class="fs-14 pt-2 pt-md-0">
                                                    Getting groceries at home is made simpler by BigBasket.
                                                    This online megastore offers farm fresh veggies, fruits, dairy products, and more to your doorstep.
                                                    Discover daily essentials and gourmet food with 20k plus options to choose from.
                                                    At Gifthos, we offer BigBasket gift cards at discounted prices for you.
                                                    Redeem your gift card easily on your online orders.</p>
                                            </div>
                                            <div class="bottom-left-offers">
                                                <a href="#">
                                                    <p style="color:red;">Read More</p>
                                                </a>

                                            </div>
                                        </div>
                                        <a class="carousel-control-prev" href="#product-carousel" data-slide="prev">
                                            <i class="fas fa-2x fa-angle-left text-dark"></i>
                                        </a>
                                        <a class="carousel-control-next" href="#product-carousel" data-slide="next">
                                            <i class="fas fa-2x fa-angle-right text-dark"></i>
                                        </a>
                                    </div>
                                    <!-- <p class="mb-4 mb-md-0">
                                        Getting groceries at home is made simpler by BigBasket.
                                        This online megastore offers farm fresh veggies, fruits, dairy products, and more to your doorstep.
                                        Discover daily essentials and gourmet food with 20k plus options to choose from.
                                        At Gifthos, we offer BigBasket gift cards at discounted prices for you.
                                        Redeem your gift card easily on your online orders.</p> -->
                                </div>
                                <div class="col-md-3 col-lg-3 col-xl-3">
                                    <div class="col-lg-4 col-md-12 mb-4">
                                        <div class="bg-image hover-zoom ripple shadow-1-strong rounded">
                                            <img src="<?php echo base_url() ?>/assets/img/ven1.png" class="" />
                                            <a href="#!"></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Code for 4 buttons options -->
            <div class="row justify-content-center ">
                <div class="col-12 col-md-12 col-lg-12 col-xl-12 ml-0 m-2">
                    <div class=" card shadow-0 border-0 ">
                        <div class="card-body">

                            <div class="btn-group col-12 ">
                                <button type="button" class="btn btn-outline-secondary1 mr-2 col"><img src="<?php echo base_url() ?>/assets/img/bb.png">
                                    Instructions</button>
                                <button type="button" class="btn btn-outline-secondary1 mr-2 col"><img src="<?php echo base_url() ?>/assets/img/vv2.png">
                                    Store Locator</button>
                                <button type="button" class="btn btn-outline-secondary1 mr-2 col"><img src="<?php echo base_url() ?>/assets/img/tc3.png">T&C</button>
                                <button type="button" class="btn btn-outline-secondary1 mr-2 col"><img src="<?php echo base_url() ?>/assets/img/tc1.png">
                                    How to Redeem</button>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <!--  End of Code for 4 buttons options -->
        </div>


        <div class="container-fluid py-2 px-5">

            <div class="row pb-1">
                <!-- 1st line of coupons -->
                <div class="col-8 col-md-8 col-lg-8" style="background-color:#dcd2d0" ;>
                    <div class="row pt-3" >
                        <!-- <div class="col-12"> -->
                            <!-- <div class="row p-3"> -->
                                <div class="col-sm-4 mb-3">
                                    <div class="card voucher">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="bg-white p-2 position-relative h-100 w-100 fs-16 InnerDiv">
                                                    <span class="h5 font-weight-bold">&#8377;50</span>
                                                    <div class="p-2 mb-2 bg-light text-muted" style="width:auto">Discount 12.5</div>
                                                    <div class="p-2 mb-2 bg-light text-muted" style="width:auto"> Saving</div>
                                                    <div class="p-2 mb-2 bg-light text-muted" style="width:auto">You Pay</div>
                                                    <a href="#" class="btn btn-light rounded-0 float-right">Add</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-4 mb-3">
                                    <div class="card voucher">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="bg-white p-2 position-relative h-100 w-100 fs-16 InnerDiv">
                                                    <span class="h5 font-weight-bold">&#8377;100</span>
                                                    <div class="p-2 mb-2 bg-light text-muted" style="width:auto">Discount </div>
                                                    <div class="p-2 mb-2 bg-light text-muted" style="width:auto">Saving</div>
                                                    <div class="p-2 mb-2 bg-light text-muted" style="width:auto">You Pay</div>
                                                    <a href="#" class="btn btn-light float-right">Add</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-4 mb-3">
                                    <div class="card voucher">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="bg-white p-2 position-relative h-100 w-100 fs-16 InnerDiv">
                                                    <span class="h5 font-weight-bold">&#8377;250</span>
                                                    <div class="p-2 mb-2 bg-light text-muted" style="width:auto">Discount </div>
                                                    <div class="p-2 mb-2 bg-light text-muted" style="width:auto">Saving</div>
                                                    <div class="p-2 mb-2 bg-light text-muted" style="width:auto">You Pay</div>
                                                    <a href="#" class="btn btn-light float-right">Add</a>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-4 mb-3">
                                    <div class="card voucher">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="bg-white p-2 position-relative h-100 w-100 fs-16 InnerDiv">
                                                    <span class="h5 font-weight-bold">&#8377;500</span>
                                                    <div class="p-2 mb-2 bg-light text-muted" style="width:auto">Discount 12.5</div>
                                                    <div class="p-2 mb-2 bg-light text-muted" style="width:auto"> Saving</div>
                                                    <div class="p-2 mb-2 bg-light text-muted" style="width:auto">You Pay</div>
                                                    <a href="#" class="btn btn-light float-right">Add</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-4 mb-3">
                                    <div class="card voucher">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="bg-white p-2 position-relative h-100 w-100 fs-16 InnerDiv">
                                                    <span class="h5 font-weight-bold">&#8377;1000</span>
                                                    <div class="p-2 mb-2 bg-light text-muted" style="width:auto">Discount </div>
                                                    <div class="p-2 mb-2 bg-light text-muted" style="width:auto">Saving</div>
                                                    <div class="p-2 mb-2 bg-light text-muted" style="width:auto">You Pay</div>
                                                    <a href="#" class="btn btn-light float-right">Add</a>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-4 mb-3">
                                    <div class="card voucher">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="bg-white p-2 position-relative h-100 w-100 fs-16 InnerDiv">
                                                    <span class="h5 font-weight-bold">&#8377;2000</span>
                                                    <div class="p-2 mb-2 bg-light text-muted" style="width:auto">Discount </div>
                                                    <div class="p-2 mb-2 bg-light text-muted" style="width:auto">Saving</div>
                                                    <div class="p-2 mb-2 bg-light text-muted" style="width:auto">You Pay</div>
                                                    <a href="#" class="btn btn-light float-right">Add</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            <!-- </div> -->
                        <!-- </div> -->

                        <!-- 2nd line of coupons -->
                        <!-- <div class="col-12"> -->
                            <!-- <div class="col-9 col-md-9 col-lg-9" style="background-color:#dcd2d0";> -->
                            <!-- <div class="row p-3"> -->
                                <!-- Each card -->
                                <!-- <div class="col-sm-4">
                                    <div class="card voucher">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="bg-white p-2 position-relative h-100 w-100 fs-16 InnerDiv">
                                                    <span class="h5 font-weight-bold">&#8377;500</span>
                                                    <div class="p-2 mb-2 bg-light text-muted" style="width:auto">Discount 12.5</div>
                                                    <div class="p-2 mb-2 bg-light text-muted" style="width:auto"> Saving</div>
                                                    <div class="p-2 mb-2 bg-light text-muted" style="width:auto">You Pay</div>
                                                    <a href="#" class="btn btn-light float-right">Add</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div> -->

                                <!-- Each card End-->

                                <!-- <div class="col-sm-4">
                                    <div class="card voucher">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="bg-white p-2 position-relative h-100 w-100 fs-16 InnerDiv">
                                                    <span class="h5 font-weight-bold">&#8377;1000</span>
                                                    <div class="p-2 mb-2 bg-light text-muted" style="width:auto">Discount </div>
                                                    <div class="p-2 mb-2 bg-light text-muted" style="width:auto">Saving</div>
                                                    <div class="p-2 mb-2 bg-light text-muted" style="width:auto">You Pay</div>
                                                    <a href="#" class="btn btn-light float-right">Add</a>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div> -->

                                <!-- <div class="col-sm-4">
                                    <div class="card voucher">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="bg-white p-2 position-relative h-100 w-100 fs-16 InnerDiv">
                                                    <span class="h5 font-weight-bold">&#8377;2000</span>
                                                    <div class="p-2 mb-2 bg-light text-muted" style="width:auto">Discount </div>
                                                    <div class="p-2 mb-2 bg-light text-muted" style="width:auto">Saving</div>
                                                    <div class="p-2 mb-2 bg-light text-muted" style="width:auto">You Pay</div>
                                                    <a href="#" class="btn btn-light float-right">Add</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div> -->
                            <!-- </div> -->
                        <!-- </div> -->
                    </div>


                </div>



                <!-- Cart section UI-->
                <div class="col-md-4 col-xl-4 ps-lg-0">
                    <div class="Cart ml-3 pl-3">
                        <div class="row m-0">
                            <div class="col-12 col-md-12 fs-18 py-2 text-white fw-100 " style="background-color:#5d3a26">

                                <div class="summryHeader d-flex  py-1 justify-content-center">
                                    <!-- <div> -->
                                        <span >CART
                                    <i class='fas fa-shopping-cart float-right' style="font-size: 20px;"></i>
                                    <!-- <span><i class="fa fa-shopping-cart fa-align-right" aria-hidden="true" style="color:white; float:right;"></i>
                                    </span> -->
                                    <!-- </div> -->
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="m-0 py-3 border-bottom bg-white">
                            <div class="col-8 fs-14 fw-500 text-muted">
                                You Pay
                            </div>
                        </div>
                        <div class="m-0 py-3 border-bottom bg-white">
                            <div class="col-8 fs-14 fw-500 text-muted">
                                <div class="form-check">
                                    <input type="checkbox" class="form-check-input" id="exampleCheck1">
                                    <label class="form-check-label" for="exampleCheck1"><i class="fas fa-gift mr-2"></i>I am Gifting</label>
                                </div>

                            </div>
                        </div>
                        <div class="m-0 py-3 border-bottom bg-white">
                            <div class="col-8 fs-14 fw-500 text-muted">
                                <label class="form-check-label" for="exampleCheck1">
                                    Email</label>


                            </div>
                        </div>
                        <div class="p-md-3 d-grid bg-white">
                            <div class="text-center">
                                <div class="btn py-1  btn-light rounded-0">
                                    View/Edit Cart
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!--  End of Cart section UI-->

        </div>
        <!-- End of 1st line of coupons -->

        <!-- 2nd line of coupons -->
        <!-- <div class="row">
                <div class="col-9 col-md-9 col-lg-9" style="background-color:#dcd2d0";>
                    <div class="row" style="padding-left:80px";>-->
        <!-- Each card -->
        <!-- <div class="col-sm-4">
                            <div class="card voucher">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="bg-white p-2 position-relative h-100 w-100 fs-16 InnerDiv">
                                            <span class="h5 font-weight-bold">&#8377;500</span>
                                            <div class="p-2 mb-2 bg-light text-muted" style="width:auto">Discount 12.5</div>
                                            <div class="p-2 mb-2 bg-light text-muted" style="width:auto"> Saving</div>
                                            <div class="p-2 mb-2 bg-light text-muted" style="width:auto">You Pay</div>
                                            <a href="#" class="btn btn-light float-right">Add</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> -->

        <!-- Each card End-->

        <!-- <div class="col-sm-4">
                            <div class="card voucher">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="bg-white p-2 position-relative h-100 w-100 fs-16 InnerDiv">
                                            <span class="h5 font-weight-bold">&#8377;1000</span>
                                            <div class="p-2 mb-2 bg-light text-muted" style="width:auto">Discount </div>
                                            <div class="p-2 mb-2 bg-light text-muted" style="width:auto">Saving</div>
                                            <div class="p-2 mb-2 bg-light text-muted" style="width:auto">You Pay</div>
                                            <a href="#" class="btn btn-light float-right">Add</a>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div> -->

        <!-- <div class="col-sm-4">
                            <div class="card voucher">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="bg-white p-2 position-relative h-100 w-100 fs-16 InnerDiv">
                                            <span class="h5 font-weight-bold">&#8377;2000</span>
                                            <div class="p-2 mb-2 bg-light text-muted" style="width:auto">Discount </div>
                                            <div class="p-2 mb-2 bg-light text-muted" style="width:auto">Saving</div>
                                            <div class="p-2 mb-2 bg-light text-muted" style="width:auto">You Pay</div>
                                            <a href="#" class="btn btn-light float-right">Add</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> -->
        <!-- </div>
                </div>
            </div>  -->
        </div>


        <!-- End of 2nd line of coupons -->

        <!-- Featured Start -->

        <div class="container-fluid1 mt-5 pt-3 py-2 pt-2 mt-3">

            <!-- Content to be scrolled -->
            <div class="row px-xl-5 bg-white pt-3 pb-0">
                <div class="scroll7 w-100">
                    <div class="col-lg-3 col-md-6 col-sm-12">
                        <div class="d-flex align-items-center  mb-4">
                            <div class="card-heade1 product-img position-relative overflow-hidden">
                                <img src="<?php echo base_url() ?>/assets/img/1.png">
                            </div>

                        </div>
                    </div>

                    <div class="col-lg-3 col-md-6 col-sm-12">
                        <div class="d-flex align-items-center  mb-4">
                            <div class="card-header1 product-img position-relative ">
                                <img src="<?php echo base_url() ?>/assets/img/2.png">
                            </div>

                        </div>
                    </div>

                    <div class="col-lg-3 col-md-6 col-sm-12">
                        <div class="d-flex align-items-center  mb-4">
                            <div class="card-header1 product-img position-relative overflow-hidden">
                                <img src="<?php echo base_url() ?>/assets/img/3.png">
                            </div>

                        </div>
                    </div>

                    <div class="col-lg-3 col-md-6 col-sm-12">
                        <div class="d-flex align-items-center  mb-4">
                            <div class="card-header1 product-img position-relative overflow-hidden">
                                <img src="<?php echo base_url() ?>/assets/img/4.png">
                            </div>
                        </div>

                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-12">
                        <div class="d-flex align-items-center  mb-4">
                            <div class="card-header1 product-img position-relative overflow-hidden">
                                <img src="<?php echo base_url() ?>/assets/img/5.png">
                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </div>


        </div>




        </div>

        </div>


        </div>
        <!-- Featured End -->
        </div>

    </section>




    <!-- Vendor Start -->
    <div class="container-fluid py-2 pt-2 mt-3">
        <div class="row px-xl-2">
            <div class="col">
                <div class="owl-carousel vendor-carousel">
                    <div class="vendor-item border p-4">
                        <img src="<?php echo base_url() ?>/assets/img/vendor-1.jpg" alt="">
                    </div>
                    <div class="vendor-item border p-4">
                        <img src="<?php echo base_url() ?>/assets/img/vendor-2.jpg" alt="">
                    </div>
                    <div class="vendor-item border p-4">
                        <img src="<?php echo base_url() ?>/assets/img/vendor-3.jpg" alt="">
                    </div>
                    <div class="vendor-item border p-4">
                        <img src="<?php echo base_url() ?>/assets/img/vendor-4.jpg" alt="">
                    </div>
                    <div class="vendor-item border p-4">
                        <img src="<?php echo base_url() ?>/assets/img/vendor-5.jpg" alt="">
                    </div>
                    <div class="vendor-item border p-4">
                        <img src="<?php echo base_url() ?>/assets/img/vendor-6.jpg" alt="">
                    </div>
                    <div class="vendor-item border p-4">
                        <img src="<?php echo base_url() ?>/assets/img/vendor-7.jpg" alt="">
                    </div>
                    <div class="vendor-item border p-4">
                        <img src="<?php echo base_url() ?>/assets/img/vendor-8.jpg" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Vendor End -->
</body>

</html>